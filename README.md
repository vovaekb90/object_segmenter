# object_segmenter package #

Package for segmenting object from a point cloud using point indices.


## Usage of package ##

### Prerequisites ###
Prerequisites for using package:

* point cloud
* file with point indices of objects (txt)

Example of file with point indices:


```
#!text

91870
91871
92509
92510
92511
...
```


### Run package ###
Add package to catkin_ws.
Build catkin workspace:


```
#!Bash

cd ~/catkin_ws
catkin_make
```

Source catkin workspace:


```
#!Bash

source devel/setup.bash
```

Run package:


```
#!Bash

rosrun object_segmenter segment_object <point_cloud>.pcd --indices <indices_file>.txt
```

Result of work:

![object_segmenter_result.png](https://bitbucket.org/repo/KrML5Lj/images/4172871832-object_segmenter_result.png)

Test example of point cloud is located in directory **test**.